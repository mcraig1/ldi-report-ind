cap-status	cap	cap-group	cap-reflect	cap-def	cap-rows	cap-order
Active	Agency	cap-sl	"Consider the factors that have influenced your success. Which of these factors are outside forces, and which are under your control?"	Ability to initiate and sustain goal-directed efforts	I energetically pursue my goals	26.1
Active	Agency		"As you set future goals, what lessons can you apply from prior successes and failures?"		My past experiences have prepared me well for my future	26.2
Active	Agency		"In times where you haven't felt you could be successful in meeting your goals, what helped you persist?"		I've been pretty successful in life	26.3
Active	Agency		What motivates you in setting and achieving your goals? How has this changed over time?		I meet the goals that I set for myself	26.4
Active	Civic engagement	cap-cl	How do you define the concept of community? What obligations do you feel towards communities of which you are a part?	Desire and ability to contribute to one's community	I believe I have responsibilities to my community	31.1
Active	Civic engagement		What are the biggest challenges facing one of the communities? What might you do to increase your awareness of community issues?		I work with others to make my communities better places	31.2
Active	Civic engagement		How do you invest energy in different communities and what does that mean for how you show up?		I participate in activities that contribute to the common good	31.3
Active	Civic engagement		To what extent is there a gap between what you believe about community and the degree to which you take action on those beliefs?		I value opportunities that allow me to contribute to my community	31.4
Active	Civic engagement		What relationships would you need to establish to improve your community?		It is important to me that I play an active role in my communities	31.5
Active	Civic engagement				I believe my work has a greater purpose for the larger community	31.6
Active	Collaboration	cap-iperl	Do you notice how your individual efforts impact the result of a group effort? What can you learn from these types of observations?	Desire and ability to work effectively with others in group processes	I am seen as someone who works well with others	7.1
Active	Collaboration		Write down 5 or more challenges a team you are part of currently faces. Underline the challenges you would prefer to address on your own. Circle those you would prefer to address with a group. What differentiates your preferences? Are there areas where both approaches would be beneficial?		I can make a difference when I work with others on a task	7.2
Active	Collaboration		"Reflect on a time when a group experience went well. Identify 3 factors that contributed to the success. Now think about a time when a group experience went poorly. What, if anything, was different about that experience?"		I actively listen to what others have to say	7.3
Active	Collaboration		What are three actions that you can take in the next week to improve how you collaborate in teams?		I enjoy working with others toward common goals	7.4
Active	Collaboration				Others would describe me as a cooperative group member	7.5
Active	Collaboration				My contributions are recognized by others in the groups I belong to	7.6
Active	Critical questioning	cap-innol	Reflect on a recent interaction you had with a colleague or friend. What types of questions did you ask? What kinds of questions added to the productivity of the conversation?	The ability to generate a variety of high-quality questions as well as strategically deploy questions to build relationships and acquire information.	I ask different types of questions in different situations to understand the root of a topic	15.1
Active	Critical questioning		"When you are learning about a new topic of interest, what types of questions do you ask to understand the topic in more depth?"		"I feel comfortable asking questions, even if I am the only one doing so"	15.2
Active	Critical questioning		"Identify an important conversation you have coming up this week. Map the goals of that conversation, what success looks like, and the types of questions that will get you there."		I know how to strategically ask questions in different situations	15.3
Active	Critical questioning				I'm skilled at using open-ended questions to push a conversation deeper	15.4
Active	Cultural competence	cap-incl	Reflect on your network of friends and colleagues. What cultural beliefs do you share with that network and which are individual to you? How do commonalities and differences influence your relationships?	"Requisite awareness, values, and behaviors that enable effective communication and advocacy across cultures"	I regularly think about how my cultural beliefs impact those from cultures different than my own	19.1
Active	Cultural competence		What proactive steps can you take to learn about cultures different from your own?		"I can readily explain how systemic factors (e.g., racism, sexism, poverty) influence varying cultures' experience"	19.2
Active	Cultural competence		"Consider the last time you received feedback regarding an interaction across differences (e.g., identity, culture, beliefs). What did you feel? How did you respond?"		I actively educate myself about the struggles of those from cultures different than my own	19.3
Active	Cultural competence				I am effective engaging across cultural differences	19.4
Active	Cultural competence				I can self-correct after receiving feedback about interactions across differences	19.5
Active	Curiosity	cap-innol	Think of times when you encountered novel situations or ideas and found them exciting versus confusing. What factors contributed to each reaction and what might this tell you about what stimulates your curiosity?	"Pursuit of knowledge or experiences through novel, complex, differing, or uncertain stimuli to resolve gaps, understand, and learn"	I engage in opportunities I know little about to learn more	13.1
Active	Curiosity		How often do you put yourself in environments where you will be exposed to truly novel thinking?		I actively seek out opportunities to learn something new	13.2
Active	Curiosity		"What does ""curiosity in action"" look like to you? How do you move from intellectual curiosity to practical actions?"		"After learning something new, I look for more information to fill gaps that were not answered"	13.3
Active	Curiosity				I place myself in contexts that will expose me to new ideas	13.4
Active	Emotional self-awareness	cap-pl	Think of a time when your emotional response caught you off guard. What was the situation? How did you react? What was the impact? What lessons can you apply from this to inform your leadership development?	Ability to accurately recognize one's emotions and their influences	I'm able to accurately describe how my feelings impact my actions	4.1
Active	Emotional self-awareness		Consider a recent interaction with a peer where you experienced joy. Now consider an interaction with the same person where you experienced sadness. How did those emotions change your interactions?		I recognize the connection between what is happening around me and how it shapes my feelings	4.2
Active	Emotional self-awareness		"Psychologist, Dr. Susan David once said, ""emotions are data points, not directives."" How does that statement align with your result on this capacity? How might it inform your growth?"		I can accurately describe my feelings in ways that other people can understand	4.3
Active	Emotional self-awareness				"Typically, I understand the underlying reasons for my feelings"	4.4
Active	Emotional self-regulation	cap-pl	List 3-5 situations that are especially stressful for you. What do these situations have in common?	The ability to use emotions to inform one's impressions of an experience while intentionally choosing actions to pursue healthy and effective ways forward.	I proactively remind myself to remain calm when I know a situation may become stressful	5.1
Active	Emotional self-regulation		What strategies do you use to stay calm in a stressful situation? Rank your top five most effective.		I'm able to perform effectively even under pressure	5.2
Active	Emotional self-regulation		How do you know when you are feeling stressed or about to lose your composure? What do you do when you have this realization?		I can identify when I have impulsive reactions	5.3
Active	Emotional self-regulation		"How does your ability to regulate your emotions change under pressure? What is the impact of this on your goal-achievement, team, and relationships?"		I can manage my emotional response in healthy ways	5.4
Active	Experimentation	cap-innol	Consider a time when you experienced failure. What did you learn from that moment? How does the lesson change if you reframe the experience as a learning opportunity?	Ability to suspend judgment by hypothesis testing through active experimentation to generate unique insights	I know how to test ideas in different ways	16.1
Active	Experimentation		"How do you stay open to multiple possibilities when exploring an idea even under constraints (e.g., time, financial)? What value does this bring to leadership processes?"		I see failure as a starting point rather than an endpoint	16.2
Active	Experimentation		"Identify a commonly held assumption that you ""know"" to be true (e.g, management doesn't care about employees, Jenny lacks problem-solving skills). Over the course of the week, test this assumption. Consciously look for evidence that contradicts what you ""know"" to be true. What value does this bring to your leadership development?"		I generate multiple possibilities rather than committing to a singular path forward	16.3
Active	Experimentation				I don't let my preconceived notions get in the way of experimenting with new ideas	16.4
Active	Generativity	cap-cl	How did previous generations work to provide a better life for you?	"Concern for future generations as well as engagement in current actions to
advance the future of a community"	I carry out activities to ensure a better world for future generations	32.1
Active	Generativity		What are the top 3 sacrifices you could make today to create a better tomorrow? How do you strategically make decisions about risks associated with sacrifice?		I have a personal responsibility to improve the area in which I live	32.2
Active	Generativity		How do you create space in your life to consider long-term responsibilities? What do you need to do today so your goals come to fruition 10 years from now?		I give up part of my daily comforts to foster the development of next generations	32.3
Active	Generativity				I think that I am responsible for ensuring a state of well-being for future generations	32.4
Active	Integrative thinking	cap-innol	"What strategies do you use to recognize patterns across relationships, situations, or ideas? How do you apply this information in leadership processes?"	Ability to engage in non-linear thinking to make connections across traditional boundaries and seemingly unrelated phenomena	I am able to take seemingly unrelated items and find a connection	18.1
Active	Integrative thinking		Think of a time when a process felt chaotic for you while others in the group felt things were going well. What made it feel chaotic? To what extent is there a value in this?		I easily think of analogies when explaining an idea	18.2
Active	Integrative thinking		"When you become stuck on a problem, what steps do you take to become unstuck? Draw a flow chart of your approach identifying multiple alternatives at each major milestone."		I typically see multiple starting points for how best to understand an issue	18.3
Active	Integrative thinking				I value time for free-flowing idea generation without the expectation for an immediate solution or application	18.4
Active	Integrative thinking					18.5
Active	Integrity	cap-pl	Identify your top 10 personal values. Rank order this list identifying your top 3 values. How do you communicate these values with others?	"Alignment between convictions (i.e., beliefs, values, attitudes) and actions"	My behaviors are congruent with my beliefs	2.1
Active	Integrity		"Conduct an audit of your behaviors. Start by identifying core beliefs. Over the past week, when and how have your behaviors been aligned and/or misaligned with those beliefs? What lessons can be distilled from this?"		It is important to me to act on my beliefs	2.2
Active	Integrity		How do you hold yourself accountable for ensuring that your values and beliefs are congruent as often as possible?		My actions are consistent with my values	2.3
Active	Integrity				Being seen as a person of integrity is important to me	2.4
Active	Integrity				My behaviors reflect my beliefs	2.5
Active	Leadership efficacy	cap-sl	Identify a time when you felt successful leading a group or team. What factors contributed to the sense that you could be successful?	One's internal beliefs about their likelihood of success when engaging in leadership processes	Leading others	27.1
Active	Leadership efficacy		What differentiates confidence from over-confidence in your mind? Where have you crossed those lines in your life and what was the impact?		Organizing a group's tasks to accomplish a goal	27.2
Active	Leadership efficacy		"What positive and negative ""self-talk"" goes through your mind when engaging in leader roles? What strategies have you used to quiet negative ""self-talk""?"		Taking initiative to improve something	27.3
Active	Leadership efficacy				Working with a team on a group project	27.4
Active	Navigating systems	cap-incl	How quickly do you change course when encountering a problem? How do you identify the right timing to shift strategies?	Ability to envision multiple pathways in navigating systems to achieve goals	I can think of many ways to get out of a jam	24.1
Active	Navigating systems		What role does contingency planning play even before you've hit a roadblock?		There are lots of ways around any problem	24.2
Active	Navigating systems		What motivating factors do you draw on to persist past obstacles? How do you apply this to leadership processes?		I can think of many ways to get the things in life that are important to me	24.3
Active	Navigating systems				"Even when others get discouraged, I know I can find a way to solve a problem"	24.4
Active	Open-mindedness	cap-iperl	"Audit your receptivity to differing opinions. Over the course of the week, note when disagreements occur. When did this feel like a healthy versus unhealthy disagreement? What role did you play in each of these situations?"	Receptivity to and appreciation of differing viewpoints and the advantages of engaging with them in productive ways	I am open to others' ideas	10.1
Active	Open-mindedness		Identify 5 ways in which you proactively demonstrate openness to differences of opinion in group contexts.		I value differences in others	10.2
Active	Open-mindedness		What responsibility do you have in co-creating an environment that supports divergent ideas? How does this shape your approach to leading individuals and teams?		Hearing differences in opinions enriches my thinking	10.3
Active	Open-mindedness				I respect opinions other than my own	10.4
Active	Open-mindedness				I share my ideas with others	10.5
Active	Perceptiveness		How can you train your brain to see beyond the obvious?	The ability to perceive beyond the expected using all senses to heighten awareness in a given moment or context.	I enjoy exploring beyond the obvious in a situation	17.1
Active	Perceptiveness		"Look around the space that you are in right now; what do you see? If this is a space you are in often, stretch yourself to notice at least 10 new things."		I notice things that others do not when looking at the same thing	17.2
Active	Perceptiveness		"As you observe the space you are in, which senses do you most rely on to describe the space? Pick a non-dominant sense that you rely on less frequently. Identify 3-5 new observations."		"When I see something (e.g., artwork, people, readings) repeatedly, I usually notice something new"	17.3
Active	Perceptiveness				"I like studying a new experience, person, or context to see what I can learn from it"	17.4
Active	Personal responsibility	cap-pl	What does personal responsibility mean to you? How do you hold yourself accountable to your responsibilities?	Commitment to directing one's energy and actions toward agreed-upon efforts	I am willing to devote the time and energy to things that are important to me	3.1
Active	Personal responsibility		"What role does dependability play in groups and teams? Conduct a dependability ""audit"" by tracking times that you deliver against responsibilities as well as those when you fail. What lessons can be learned from this?"		I stick with others through difficult times	3.2
Active	Personal responsibility		How do you show up for peers through difficult times including through conflict in your relationship?		I am focused on my responsibilities	3.3
Active	Personal responsibility				I can be counted on to do my part	3.4
Active	Personal responsibility				I follow through on my promises	3.5
Active	Personal responsibility				I hold myself accountable for responsibilities I agree to	3.6
Active	Resilience	cap-sl	What coping mechanism do you use to deal with stress and pressure? In what ways can you build on your existing assets to develop new coping mechanisms?	Ability to persist in the midst of adversity and positively cope with stress	I can deal with whatever comes my way	28.1
Active	Resilience		"Consider the role that feeling discouraged plays in your life. What thoughts, feelings, and actions most typically accompany this feeling? To what extent are these helpful or harmful to your relationships and goals?"		Having to cope with stress can make me stronger	28.2
Active	Resilience		"When you encounter challenges, what strategies do you use to maintain focus on your goals?"		"Under pressure, I stay focused and think clearly"	28.3
Active	Resilience				I am not easily discouraged by failure	28.4
Active	Resilience				I think of myself as a strong person when dealing with life's challenges and difficulties	28.5
Active	Resilience				"I am able to handle unpleasant or painful feelings like sadness, fear, and anger"	28.6
Active	Search for purpose	cap-sl	Why might it be important to have a sense of purpose? What role does purpose play in your decision-making?	Meaning-making grounded in the exploration of life's deeper purpose	How often do you...search for meaning/purpose in your life?	29.1
Active	Search for purpose		Think of three people with whom you are close. How have you discussed your own search for purpose with those people? What benefits accrue from these types of conversations?		have discussions about the meaning of life with your friends?	29.2
Active	Search for purpose		How do you allow your purpose to evolve over time? What role do friends and colleagues play in influencing this?		surround yourself with friends who are searching for meaning/purpose in life?	29.3
Active	Search for purpose				reflect on finding answers to the mysteries of life?	29.4
Active	Search for purpose				Think about developing a meaningful philosophy of life?	29.5
Active	Self-awareness	cap-pl	Consider what self-awareness means to you. What factors contribute to a person being self-aware?	"Understanding of and ability to express one's sense of self, values, and priorities"	I am able to articulate my priorities	1.1
Active	Self-awareness		What insights and blindspots may influence your level of self-awareness?		I am usually self-confident	1.2
Active	Self-awareness		"Identify three people who you would consider ""comfortable in their own skin."" Observe how they express themselves. How is their self-awareness evident?"		I know myself pretty well	1.3
Active	Self-awareness		What would you identify as your core priorities in life? How do they reflect your core values and belief systems?		I could describe my personality	1.4
Active	Self-awareness				I can describe how I am similar to other people	1.5
Active	Self-awareness				I am comfortable expressing myself	1.6
Active	Sense of belonging	cap-iperl	"Reflect on a time when you felt ""at home"" with a group. What contributed to your feeling in that moment?"	Degree to which an individual feels a sense of affiliation and connection to an organization/program	I feel valued as a person in my community	12.1
Active	Sense of belonging		List three actions you can do to make others feel connected to a group of which you are a part.		I feel accepted as a part of my community	12.2
Active	Sense of belonging		What responsibilities do you have to contribute to your own sense of belonging? What does this require of you? How do you invite others into this process?		I feel I belong in my community	12.3
Active	Social capital creation	cap-cl	How do you prioritize the development of relationships with others while managing existing obligations?	"Ability to develop and maintain diverse relationships across traditional boundaries for the purpose of mutual benefit in personal, community, and professional domains"	I intentionally seek out relations with others who are different than myself	34.1
Active	Social capital creation		What strategies do you use to create connections with others in a similar career field? Are those different from how you connect with people in different career fields?		"When talking with someone, I look for connections that may either help me or help them"	34.2
Active	Social capital creation		"Identify a mentor who you know and seek out their social media account. From their list of contacts, identify 1-2 people who you would like to connect with and have mutual interests. Reach out in the next week."		I can effectively build a network in professional fields outside my own	34.3
Active	Social capital creation				I'm skilled at identifying mutual interests between colleagues and/or friends	34.4
Active	Social change behaviors	cap-incl	How do you move from curiosity and passion into action around a social change issue? What constrains your ability to take action and what enables it?	Enactment of values-based leadership and civic engagement behaviors to advance the common good	Performed community service	22.01
Active	Social change behaviors				Acted to benefit the common good or protect the environment	22.02
Active	Social change behaviors				Been actively involved with an organization that addresses a social or environmental problem	22.03
Active	Social change behaviors				Been actively involved with an organization that addresses the concerns of a specific community	22.04
Active	Social change behaviors				Communicated with community leaders about a pressing concern	22.05
Active	Social change behaviors				Took action in the community to try and address a social or environmental problem	22.06
Active	Social change behaviors				Worked with others to make the community a better place	22.07
Active	Social change behaviors				"Acted to raise awareness about a local, national, or global problem"	22.08
Active	Social change behaviors				"Took part in a protest, rally, march, or demonstration"	22.09
Active	Social change behaviors				Worked with others to address social inequality	22.1
Active	Social perspective-taking	cap-incl	What strategies do you employ to understand viewpoints different from your own?	Ability to take another person's point of view and accurately infer their thoughts and feelings	I try to look at everybody's side of a disagreement before I make a decision	21.1
Active	Social perspective-taking		What emotions do you feel when other people don't understand your point of view in a discussion? How do you navigate this? What are the implications for how you practice leadership?		I sometimes try to understand my friends better by imagining how things look from their perspective	21.2
Active	Social perspective-taking		What tactics have you used to build your emotional intelligence and capacity for empathy? How does this inform your approach to leadership?		I believe that there are two sides to every question and try to look at them both	21.3
Active	Social perspective-taking				"When I'm upset at someone, I usually try to ""put myself in their shoes"" for awhile"	21.4
Active	Social perspective-taking				"Before criticizing somebody, I try to imagine how I would feel if I were in their place"	21.5
Active	Sociocultural discussion	cap-incl	How often do you engage in discussions with people who hold viewpoints different from your own? What factors contribute to positive or negative experiences across differences for you?	"Engagement with peers around compelling social and cultural issues including
values stances, diversity, human rights, political perspectives, and religious beliefs"	Talked about different lifestyles/customs	23.1
Active	Sociocultural discussion		How do you ensure that people with identities or cultures different from your own do not bear the responsibility of educating you?		Held discussions with others whose personal values were very different from your own	23.2
Active	Sociocultural discussion		Conversations about and across differences are among the most influential means to increase leadership capacity scores. Why do you think this is the case? Identify three strategies for increasing your exposure to conversations about and across differences during the next month.		"Discussed major social issues such as peace, human rights, and justice"	23.3
Active	Sociocultural discussion				Held discussions with others whose religious beliefs were very different from your own	23.4
Active	Sociocultural discussion				Discussed your views about multiculturalism and diversity	23.5
Active	Sociocultural discussion				Held discussions with others whose political opinions were very different from your own	23.6
Hold	Adaptability	cap-sl	NA for beta			25
Hold	Leadership aspirations	cap-cl	NA for beta			33
Hold	Leadership motivation	cap-cl	NA for beta			30
Hold	Role modeling	cap-iperl	NA for beta			11
7th Measure	Creative problem-solving	cap-innol	NA for beta			14
7th Measure	Diversity appreciation	cap-incl	NA for beta			20
7th Measure	Learning efficacy	cap-pl	NA for beta			6
7th Measure	Teaming		NA for beta			8
7th Measure	Mentoring		NA for beta			9