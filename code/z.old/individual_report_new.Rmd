---
title: "ALDI Individual Report"
output:
  html_document: 
    theme: null
    css: report_style.css
    smart: no
---

```{r setup, echo=FALSE}
library(htmlTable)
library(ggcharts)
```

<!----CAPACITIES PAGE---->
<!--HEADER-->
<div id='header-container' class='break'>
<table id='header-table'>
<tr>
<td style="vertical-align: bottom;padding-bottom: .5em;width: 7in;"><p><strong>`r usr.name`</strong> `r format(Sys.Date(), '%m/%d/%Y')`</p>
<div style='width:3%;background-color:rgb(207,144,38);height:.5em;float:left;'></div><div style='width:97%;background-color:rgb(0,73,123);height:.5em;'></div>
</td>
<td><img src="C:/reports/images/index-primary-cropped.png" id='page-logo'/></td>
</tr>
</table>
</div>

<!--CAPACITIES WHEEL-->
<div id='overview-top-box'>
<div id='overview-top-left'>
<h1>About the Aspen Index</h1>
<p>Your Aspen Index: Individual Report is generated based on responses you provided during the survey. Your report covers six leadership capacities from among 30+ options that appear in the image to the right. A full list of capacities and their definitions appears in the back of this report.</p> 
<p>Think of the pages that follow as an inventory of your existing assets and areas for growth. To learn more about the psychometric rigor of the Aspen Index and its influence in the field of leadership development, visit our website at: <a href="https://www.aspenindex.org" target="_blank">www.aspenindex.org</a>.</p>
</p>
</div>
<div id='overview-top-circle'>
<img src='C:/reports/images/index-circle.png' id='index-circle'/>
</div>
</div>


<!--CHOSEN CAPACITIES-->
<div id='overview-bttm-box'>
<table id='your-index-table'>
<tr>
<td colspan='3'><h1>Your Aspen Index.</h1> <span style='color:#00497b'>You answered questions on scales that measure the following six leadership capacities:</span></td>
</tr>
<tr>
<td><div class='y-index-header `r db.capsumm$cap_group[1]`'>`r db.capsumm$cap_group_name[1]`</div><strong>`r db.capsumm$cap[1]`</strong><br/>`r db.capsumm$cap_def[1]`</div></td>
<td><div class='y-index-header `r db.capsumm$cap_group[2]`'>`r db.capsumm$cap_group_name[2]`</div><strong>`r db.capsumm$cap[2]`</strong><br/>`r db.capsumm$cap_def[2]`</div></td>
<td><div class='y-index-header `r db.capsumm$cap_group[3]`'>`r db.capsumm$cap_group_name[3]`</div><strong>`r db.capsumm$cap[3]`</strong><br/>`r db.capsumm$cap_def[3]`</div></td>
</tr><tr>
<td><div class='y-index-header `r db.capsumm$cap_group[4]`'>`r db.capsumm$cap_group_name[4]`</div><strong>`r db.capsumm$cap[4]`</strong><br/>`r db.capsumm$cap_def[4]`</div></td>
<td><div class='y-index-header `r db.capsumm$cap_group[5]`'>`r db.capsumm$cap_group_name[5]`</div><strong>`r db.capsumm$cap[5]`</strong><br/>`r db.capsumm$cap_def[5]`</div></td>
<td><div class='y-index-header `r db.capsumm$cap_group[6]`'>`r db.capsumm$cap_group_name[6]`</div><strong>`r db.capsumm$cap[6]`</strong><br/>`r db.capsumm$cap_def[6]`</div></td>
</tr>
</table>
</div>

<!----DEF PAGE---->
<!--HEADER-->
<div id='header-container' class='break'>
<table id='header-table'>
<tr>
<td style="vertical-align: bottom;padding-bottom: .5em;width: 7in;"><p><strong>`r usr.name`</strong> `r format(Sys.Date(), '%m/%d/%Y')`</p>
<div style='width:3%;background-color:rgb(207,144,38);height:.5em;float:left;'></div><div style='width:97%;background-color:rgb(0,73,123);height:.5em;'></div>
</td>
<td><img src="C:/reports/images/index-primary-cropped.png" id='page-logo'/></td>
</tr>
</table>
</div>


<h1 style='margin-top:-.75in;'>How to Read Your Report</h1>
<div style='font-size:10pt;'>
The Aspen Index first measures your performance across six leadership capacities. It then compares your individual results with established national and career-specific benchmarks to identify your assets and opportunities for growth. 

Your report includes a results page for each capacity with information broken down into four sections:<ol>
<li><strong>Bar Graph:</strong> A visual representation of your score along with the national average score and the selected cohort average score for comparison purposes.</li>
<li><strong>Interpreting Your Results</strong> Step-by-step walkthrough for reading, understanding, and acting on your results.</li>
<li><strong>Elements of a Capacity:</strong> Table with scores for each essential element of a capacity along with the corresponding national average scores and selected cohort average scores. </li>
<li><strong>Reflection for Action:</strong> Stimulus questions related to your assets and opportunities for growth in a particular capacity. </li></ol>


<h1>Important Terms</h1>
<p><span class='txt-larger-blue'>Capacity.</span> Capacities are the building blocks of leadership development and defined as an individual or group’s knowledge, skills, behaviors, and attitudes associated with a key leadership concept (e.g., creative problem-solving, integrity). Learning to apply capacities across contexts and in varying situations is essential for effective leadership. Capacities and their definitions appear in the top, left-hand corner of results pages. </p>

<p><span class='txt-larger-blue'>Scale.</span> Each capacity is measured using a scale composed of items that are essential elements of that capacity. Some capacities have different numbers of items as well as different response ranges (e.g., “0 - 3” or “1 – 5”), so be cautious when comparing scores across capacities. The response range used for each scale is located above the bar graph on the results pages.</p>

<p><span class='txt-larger-blue'>Score.</span> Bar graph numbers represent your overall score on a capacity along with benchmark scores for your selected cohort and the nation. Scores are calculated by taking the average of all items that compose a scale. Scores for each individual item appear in the Elements of A Capacity table. They provide indicators of your existing assets as well as actionable targets for growth within a capacity.</p>

<p><span class='txt-larger-blue'>Selected Cohort.</span> Selected cohorts reflect a narrower comparison group than national benchmark scores. Your selected cohort is tied to a specific career field and reflects the average score nationally within that career field.</p>

<table id='results-explained' style="border-collapse:collapse;">
<tr><td colspan = 3 style='border-bottom:5px solid rgb(0,73,123);'>
<h1>Your Results Explained</h1>
</td></tr>
<tr>
<td class='bg-light-blue' style='width:75%;border-right:5px solid white;padding:.5em;'>
There are two areas with explanations of your results: (1) the bolded phrase at the top of the “Interpreting Your Results” box, which explains your overall score on that capacity, and (2) the bolded phrase in the “Action” column, which explains your score on an element of a capacity.</td>
<td class='bg-light-blue'>&nbsp;</td>
<td class='bg-light-blue'>&nbsp;</td>
</tr><tr>
<td class='bg-light-blue' style='border-right:5px solid white;padding:.5em;'>
<h3>Own it</h3>
Your score is higher than the national benchmark placing you in the top 38th percentile of scores nationally. This reflects a leadership asset.</td>
<td class='bg-light-blue' style='width:7%;'><img class='comp-icon-small' src='C:/reports/images/own-it.png'/></td>
<td class='bg-light-blue'>Your score is <strong>greater than</strong> the national average. Own it.</td>
</tr><tr>
<td class='bg-light-blue' style='border-right:5px solid white;padding:.5em;'>
<h3>Stretch it</h3>
Your score is equivalent to the national benchmark placing you in the middle 25th percentile of scores nationally. This reflects a leadership asset that can be further developed.</td> <td class='bg-light-blue'><img class='comp-icon-small' src='C:/reports/images/stretch-it.png'/></td>
<td class='bg-light-blue'>Your score is <strong>equivalent to</strong> the national average. Stretch it.</td>
</tr><tr>
<td class='bg-light-blue' style='border-right:5px solid white;padding:.5em;'>
<h3>Grow it</h3>
Your score on the capacity is lower than the national benchmark placing you in the bottom 38th percentile nationally. This reflects a growth opportunity.
</td>
<td class='bg-light-blue'><img class='comp-icon-small' src='C:/reports/images/grow-it.png'/></td>
<td class='bg-light-blue'>Your score is <strong>equivalent to</strong> the national average. Grow it.</td>
</tr>
</table>
</div>

<!----REPORT PAGE 1---->
```{r rpt-page-1, echo=FALSE}
XX = 1
```

<!--HEADER-->
<div id='header-container' class='break'>
<table id='header-table'>
<tr>
<td style="vertical-align: bottom;padding-bottom: .5em;width: 7in;"><p><strong>`r usr.name`</strong> `r format(Sys.Date(), '%m/%d/%Y')`</p>
<div style='width:3%;background-color:rgb(207,144,38);height:.5em;float:left;'></div><div style='width:97%;background-color:rgb(0,73,123);height:.5em;'></div>
</td>
<td><img src="C:/reports/images/index-primary-cropped.png" id='page-logo'/></td>
</tr>
<tr><td colspan='2'><h1>`r db.capsumm$cap[XX]`:</h1>
`r db.capsumm$cap_def[XX]`</td>
</tr>
</table>
</div>

<div id='top-container'>
<!--SCALE GRAPH-->
<div id='left-container'>
<div style='position:relative;z-index:-1;margin-left:-.7em;margin-top:-1em;'>
```{r graph1, echo=FALSE, fig.width=3.75, fig.height=4.75}
df = data.frame(x = c("self", "cohort", "national"),
                y = c(db.capsumm[XX, "self"], 
                      db.capsumm[XX, "cohort"],
                      db.capsumm[XX, "national"])
)

lollipop_chart(df, x, y, horizontal  = FALSE, point_size = 25, line_size = 15, line_color = c("#368cc3", "#1a9936", "#0e406d")) +
  annotate("text", x=1,y=df[1,2], label=df[1,2], fontface=2, size=5, colour='white') + 
  annotate("text", x=2, y=df[2,2], label=df[2,2], fontface=2, size=5, colour='white') +  
  annotate("text", x=3, y=df[3,2], label=df[3,2], fontface=2, size=5, colour='white') +
  geom_hline(yintercept = df[3,2], color = "#0e406d") +
  geom_text(aes(.6, df[3,2], label="National\nAverage", vadjust=-1), size=2) +
  ggtitle(db.capsumm$codified_scale[XX]) +
  theme(axis.text.x = element_blank(),
        axis.title.x = element_blank(),
        axis.ticks.x = element_blank(),
        axis.title.y = element_blank(),
        panel.background = element_rect(fill='white', colour = 'white'),
        plot.background = element_rect(fill='white', colour = 'white'),
        plot.title = element_text(family="sans", size=9, face='bold')) +
  expand_limits(y=c(1,5))

```
</div></div>
<!--INTERPRETATION & COHORT TEXT BOXES-->
<div id='right-container'>
<table>
<tr><td class='dark-blue-box'>Interpreting Your Results</td></tr>
<tr class='interpret-box'><td class='bg-light-blue' style='padding: .5em'>
<h3 style='margin-top:-.05in;'>Grow it</h3>
<p><div class='round-number'>1</div><div class='interpret-txt-box'><span class='txt-light-blue'>Your overall results</span><br/>
Your score is <strong>equivalent to</strong> the national benchmark score for this capacity.</p></div>
<p><div class='round-number'>2</div><div class='interpret-txt-box'><span class='txt-light-blue'>Your selected cohort results</span><br/>
Your score is <strong>equivalent to</strong> the selected cohort benchmark score for this capacity.</p></div>
<p><div class='round-number'>3</div><div class='interpret-txt-box'><span class='txt-light-blue'>Developing elements of `r db.capsumm$cap[XX]`</span> <br/>
&bull; Look at the <em>Elements of `r db.capsumm$cap[XX]`</em> column. It is ordered from your highest scored element to your lowest scored element.<br/>
&bull; Look at the <em>National Comparison</em> column. See if you scored <em>greater than, equivalent to, or less than</em> the national benchmark score for each element of `r db.capsumm$cap[XX]`. The <em>Action</em> column identifies assets and areas for growth.</p></div>
</td></tr>
<tr><td class='bg-light-green'>Your selected Cohort: <span style='color:black;'>Arts and Humanities</span></td></tr>
</table>
</div>

```{r tableoutput1, echo=FALSE}
#output table
table.builder(XX)

#build reflection items for output below
reflect.builder(db.capsumm$capacityCode[XX])
```

<div id='bttm-container' style="height:`r reflect.padding`in;">
<p style='margin-top:-.5em;font-size:12pt !important;' class='txt-larger-blue'>Reflection for Action:</p>
<ul>`r reflect.list`</ul>
</div>

<!----REPORT PAGE 2---->
```{r rpt-page-2, echo=FALSE}
XX = 2
```

<!--HEADER-->
<div id='header-container' class='break'>
<table id='header-table'>
<tr>
<td style="vertical-align: bottom;padding-bottom: .5em;width: 7in;"><p><strong>`r usr.name`</strong> `r format(Sys.Date(), '%m/%d/%Y')`</p>
<div style='width:3%;background-color:rgb(207,144,38);height:.5em;float:left;'></div><div style='width:97%;background-color:rgb(0,73,123);height:.5em;'></div>
</td>
<td><img src="C:/reports/images/index-primary-cropped.png" id='page-logo'/></td>
</tr>
<tr><td colspan='2'><h1>`r db.capsumm$cap[XX]`:</h1>
`r db.capsumm$cap_def[XX]`</td>
</tr>
</table>
</div>

<div id='top-container'>
<!--SCALE GRAPH-->
<div id='left-container'>
<div id='scale-descript-container'>`r db.capsumm$codified_scale[XX]`</div>
<!--BACKGROUND SCALE-->
<div id='scale-container'>
`r scale.builder(db.capsumm$scale[XX])`
<!--NATIONAL AVERAGE LINE-->
<div id='nat-avg-container' style="margin-top:-2.5in"><div class='nat-avg-label'>National Average</div><div class='nat-line'><hr class='nat-line-hr'/></div></div>
<!--SELF BAR-->
<div id='self-bar' class='value-bar'>
<div id='self-bar-color' class='bar-main-color'><div class='bar-background-color' style='height:1.3in'></div></div>
<div id='self-bar-value' style='margin-top:-2.8in;'><p class='bar-value-number'>1.90</p></div>
</div>
<!--COHORT BAR-->
<div id='cohort-bar' class='value-bar'>
<div id='cohort-bar-color' class='bar-main-color'><div class='bar-background-color' style='height:2.05in'></div></div>
<div id='cohort-bar-value' style='margin-top:-2in;'><p class='bar-value-number'>1.12</p></div>
</div>
<!--NATIONAL BAR-->
<div id='national-bar' class='value-bar'>
<div id='national-bar-color' class='bar-main-color'><div class='bar-background-color' style='height:2.15in'></div></div>
<div id='national-bar-value' style='margin-top:-1.85in;'><p class='bar-value-number'>1.09</p></div>
</div>
</div></div>
<!--INTERPRETATION & COHORT TEXT BOXES-->
<div id='right-container'>
<table>
<tr><td class='dark-blue-box'>Interpreting Your Results</td></tr>
<tr class='interpret-box'><td class='bg-light-blue' style='padding: .5em;'>
<h3 style='margin-top:-.05in;'>Grow it</h3>
<p><div class='round-number'>1</div><div class='interpret-txt-box'><span class='txt-light-blue'>Your overall results</span><br/>
Your score is <strong>equivalent to</strong> the national benchmark score for this capacity.</p></div>
<p><div class='round-number'>2</div><div class='interpret-txt-box'><span class='txt-light-blue'>Your selected cohort results</span><br/>
Your score is <strong>equivalent to</strong> the selected cohort benchmark score for this capacity.</p></div>
<p><div class='round-number'>3</div><div class='interpret-txt-box'><span class='txt-light-blue'>Developing elements of `r db.capsumm$cap[XX]`</span> <br/>
&bull; Look at the <em>Elements of `r db.capsumm$cap[XX]`</em> column. It is ordered from your highest scored element to your lowest scored element.<br/>
&bull; Look at the <em>National Comparison</em> column. See if you scored <em>greater than, equivalent to, or less than</em> the national benchmark score for each element of `r db.capsumm$cap[XX]`. The <em>Action</em> column identifies assets and areas for growth.</p></div>
</td></tr>
<tr><td class='bg-light-green'>Your selected Cohort: <span style='color:black;'>Arts and Humanities</span></td></tr>
</table>
</div>

```{r tableoutput2, echo=FALSE}
#output table
table.builder(XX)

#build reflection items for output below
reflect.builder(db.capsumm$capacityCode[XX])
```

<div id='bttm-container' style="height:`r reflect.padding`in;">
<p style='margin-top:-.5em;font-size:12pt !important;' class='txt-larger-blue'>Reflection for Action:</p>
<ul>`r reflect.list`</ul>
</div>

<!----REPORT PAGE 3---->
```{r rpt-page-3, echo=FALSE}
XX = 3
```

<!--HEADER-->
<div id='header-container' class='break'>
<table id='header-table'>
<tr>
<td style="vertical-align: bottom;padding-bottom: .5em;width: 7in;"><p><strong>`r usr.name`</strong> `r format(Sys.Date(), '%m/%d/%Y')`</p>
<div style='width:3%;background-color:rgb(207,144,38);height:.5em;float:left;'></div><div style='width:97%;background-color:rgb(0,73,123);height:.5em;'></div>
</td>
<td><img src="C:/reports/images/index-primary-cropped.png" id='page-logo'/></td>
</tr>
<tr><td colspan='2'><h1>`r db.capsumm$cap[XX]`:</h1>
`r db.capsumm$cap_def[XX]`</td>
</tr>
</table>
</div>

<div id='top-container'>
<!--SCALE GRAPH-->
<div id='left-container'>
<div id='scale-descript-container'>`r db.capsumm$codified_scale[XX]`</div>
<!--BACKGROUND SCALE-->
<div id='scale-container'>
`r scale.builder(db.capsumm$scale[XX])`
<!--NATIONAL AVERAGE LINE-->
<div id='nat-avg-container' style="margin-top:-2.5in"><div class='nat-avg-label'>National Average</div><div class='nat-line'><hr class='nat-line-hr'/></div></div>
<!--SELF BAR-->
<div id='self-bar' class='value-bar'>
<div id='self-bar-color' class='bar-main-color'><div class='bar-background-color' style='height:1.3in'></div></div>
<div id='self-bar-value' style='margin-top:-2.8in;'><p class='bar-value-number'>1.90</p></div>
</div>
<!--COHORT BAR-->
<div id='cohort-bar' class='value-bar'>
<div id='cohort-bar-color' class='bar-main-color'><div class='bar-background-color' style='height:2.05in'></div></div>
<div id='cohort-bar-value' style='margin-top:-2in;'><p class='bar-value-number'>1.12</p></div>
</div>
<!--NATIONAL BAR-->
<div id='national-bar' class='value-bar'>
<div id='national-bar-color' class='bar-main-color'><div class='bar-background-color' style='height:2.15in'></div></div>
<div id='national-bar-value' style='margin-top:-1.85in;'><p class='bar-value-number'>1.09</p></div>
</div>
</div></div>
<!--INTERPRETATION & COHORT TEXT BOXES-->
<div id='right-container'>
<table>
<tr><td class='dark-blue-box'>Interpreting Your Results</td></tr>
<tr class='interpret-box'><td class='bg-light-blue' style='padding: .5em;'>
<h3 style='margin-top:-.05in;'>Grow it</h3>
<p><div class='round-number'>1</div><div class='interpret-txt-box'><span class='txt-light-blue'>Your overall results</span><br/>
Your score is <strong>equivalent to</strong> the national benchmark score for this capacity.</p></div>
<p><div class='round-number'>2</div><div class='interpret-txt-box'><span class='txt-light-blue'>Your selected cohort results</span><br/>
Your score is <strong>equivalent to</strong> the selected cohort benchmark score for this capacity.</p></div>
<p><div class='round-number'>3</div><div class='interpret-txt-box'><span class='txt-light-blue'>Developing elements of `r db.capsumm$cap[XX]`</span> <br/>
&bull; Look at the <em>Elements of `r db.capsumm$cap[XX]`</em> column. It is ordered from your highest scored element to your lowest scored element.<br/>
&bull; Look at the <em>National Comparison</em> column. See if you scored <em>greater than, equivalent to, or less than</em> the national benchmark score for each element of `r db.capsumm$cap[XX]`. The <em>Action</em> column identifies assets and areas for growth.</p></div>
</td></tr>
<tr><td class='bg-light-green'>Your selected Cohort: <span style='color:black;'>Arts and Humanities</span></td></tr>
</table>
</div>

```{r tableoutput3, echo=FALSE}
#output table
table.builder(XX)

#build reflection items for output below
reflect.builder(db.capsumm$capacityCode[XX])
```

<div id='bttm-container' style="height:`r reflect.padding`in;">
<p style='margin-top:-.5em;font-size:12pt !important;' class='txt-larger-blue'>Reflection for Action:</p>
<ul>`r reflect.list`</ul>
</div>

<!----REPORT PAGE 4---->
```{r rpt-page-4, echo=FALSE}
XX = 4
```

<!--HEADER-->
<div id='header-container' class='break'>
<table id='header-table'>
<tr>
<td style="vertical-align: bottom;padding-bottom: .5em;width: 7in;"><p><strong>`r usr.name`</strong> `r format(Sys.Date(), '%m/%d/%Y')`</p>
<div style='width:3%;background-color:rgb(207,144,38);height:.5em;float:left;'></div><div style='width:97%;background-color:rgb(0,73,123);height:.5em;'></div>
</td>
<td><img src="C:/reports/images/index-primary-cropped.png" id='page-logo'/></td>
</tr>
<tr><td colspan='2'><h1>`r db.capsumm$cap[XX]`:</h1>
`r db.capsumm$cap_def[XX]`</td>
</tr>
</table>
</div>

<div id='top-container'>
<!--SCALE GRAPH-->
<div id='left-container'>
<div id='scale-descript-container'>`r db.capsumm$codified_scale[XX]`</div>
<!--BACKGROUND SCALE-->
<div id='scale-container'>
`r scale.builder(db.capsumm$scale[XX])`
<!--NATIONAL AVERAGE LINE-->
<div id='nat-avg-container' style="margin-top:-2.5in"><div class='nat-avg-label'>National Average</div><div class='nat-line'><hr class='nat-line-hr'/></div></div>
<!--SELF BAR-->
<div id='self-bar' class='value-bar'>
<div id='self-bar-color' class='bar-main-color'><div class='bar-background-color' style='height:1.3in'></div></div>
<div id='self-bar-value' style='margin-top:-2.8in;'><p class='bar-value-number'>1.90</p></div>
</div>
<!--COHORT BAR-->
<div id='cohort-bar' class='value-bar'>
<div id='cohort-bar-color' class='bar-main-color'><div class='bar-background-color' style='height:2.05in'></div></div>
<div id='cohort-bar-value' style='margin-top:-2in;'><p class='bar-value-number'>1.12</p></div>
</div>
<!--NATIONAL BAR-->
<div id='national-bar' class='value-bar'>
<div id='national-bar-color' class='bar-main-color'><div class='bar-background-color' style='height:2.15in'></div></div>
<div id='national-bar-value' style='margin-top:-1.85in;'><p class='bar-value-number'>1.09</p></div>
</div>
</div></div>
<!--INTERPRETATION & COHORT TEXT BOXES-->
<div id='right-container'>
<table>
<tr><td class='dark-blue-box'>Interpreting Your Results</td></tr>
<tr class='interpret-box'><td class='bg-light-blue' style='padding: .5em;'>
<h3 style='margin-top:-.05in;'>Grow it</h3>
<p><div class='round-number'>1</div><div class='interpret-txt-box'><span class='txt-light-blue'>Your overall results</span><br/>
Your score is <strong>equivalent to</strong> the national benchmark score for this capacity.</p></div>
<p><div class='round-number'>2</div><div class='interpret-txt-box'><span class='txt-light-blue'>Your selected cohort results</span><br/>
Your score is <strong>equivalent to</strong> the selected cohort benchmark score for this capacity.</p></div>
<p><div class='round-number'>3</div><div class='interpret-txt-box'><span class='txt-light-blue'>Developing elements of `r db.capsumm$cap[XX]`</span> <br/>
&bull; Look at the <em>Elements of `r db.capsumm$cap[XX]`</em> column. It is ordered from your highest scored element to your lowest scored element.<br/>
&bull; Look at the <em>National Comparison</em> column. See if you scored <em>greater than, equivalent to, or less than</em> the national benchmark score for each element of `r db.capsumm$cap[XX]`. The <em>Action</em> column identifies assets and areas for growth.</p></div>
</td></tr>
<tr><td class='bg-light-green'>Your selected Cohort: <span style='color:black;'>Arts and Humanities</span></td></tr>
</table>
</div>

```{r tableoutput4, echo=FALSE}
#output table
table.builder(XX)

#build reflection items for output below
reflect.builder(db.capsumm$capacityCode[XX])
```

<div id='bttm-container' style="height:`r reflect.padding`in;">
<p style='margin-top:-.5em;font-size:12pt !important;' class='txt-larger-blue'>Reflection for Action:</p>
<ul>`r reflect.list`</ul>
</div>

<!----REPORT PAGE 5---->
```{r rpt-page-5, echo=FALSE}
XX = 5
```

<!--HEADER-->
<div id='header-container' class='break'>
<table id='header-table'>
<tr>
<td style="vertical-align: bottom;padding-bottom: .5em;width: 7in;"><p><strong>`r usr.name`</strong> `r format(Sys.Date(), '%m/%d/%Y')`</p>
<div style='width:3%;background-color:rgb(207,144,38);height:.5em;float:left;'></div><div style='width:97%;background-color:rgb(0,73,123);height:.5em;'></div>
</td>
<td><img src="C:/reports/images/index-primary-cropped.png" id='page-logo'/></td>
</tr>
<tr><td colspan='2'><h1>`r db.capsumm$cap[XX]`:</h1>
`r db.capsumm$cap_def[XX]`</td>
</tr>
</table>
</div>

<div id='top-container'>
<!--SCALE GRAPH-->
<div id='left-container'>
<div id='scale-descript-container'>`r db.capsumm$codified_scale[XX]`</div>
<!--BACKGROUND SCALE-->
<div id='scale-container'>
`r scale.builder(db.capsumm$scale[XX])`
<!--NATIONAL AVERAGE LINE-->
<div id='nat-avg-container' style="margin-top:-2.5in"><div class='nat-avg-label'>National Average</div><div class='nat-line'><hr class='nat-line-hr'/></div></div>
<!--SELF BAR-->
<div id='self-bar' class='value-bar'>
<div id='self-bar-color' class='bar-main-color'><div class='bar-background-color' style='height:1.3in'></div></div>
<div id='self-bar-value' style='margin-top:-2.8in;'><p class='bar-value-number'>1.90</p></div>
</div>
<!--COHORT BAR-->
<div id='cohort-bar' class='value-bar'>
<div id='cohort-bar-color' class='bar-main-color'><div class='bar-background-color' style='height:2.05in'></div></div>
<div id='cohort-bar-value' style='margin-top:-2in;'><p class='bar-value-number'>1.12</p></div>
</div>
<!--NATIONAL BAR-->
<div id='national-bar' class='value-bar'>
<div id='national-bar-color' class='bar-main-color'><div class='bar-background-color' style='height:2.15in'></div></div>
<div id='national-bar-value' style='margin-top:-1.85in;'><p class='bar-value-number'>1.09</p></div>
</div>
</div></div>
<!--INTERPRETATION & COHORT TEXT BOXES-->
<div id='right-container'>
<table>
<tr><td class='dark-blue-box'>Interpreting Your Results</td></tr>
<tr class='interpret-box'><td class='bg-light-blue' style='padding: .5em;'>
<h3 style='margin-top:-.05in;'>Grow it</h3>
<p><div class='round-number'>1</div><div class='interpret-txt-box'><span class='txt-light-blue'>Your overall results</span><br/>
Your score is <strong>equivalent to</strong> the national benchmark score for this capacity.</p></div>
<p><div class='round-number'>2</div><div class='interpret-txt-box'><span class='txt-light-blue'>Your selected cohort results</span><br/>
Your score is <strong>equivalent to</strong> the selected cohort benchmark score for this capacity.</p></div>
<p><div class='round-number'>3</div><div class='interpret-txt-box'><span class='txt-light-blue'>Developing elements of `r db.capsumm$cap[XX]`</span> <br/>
&bull; Look at the <em>Elements of `r db.capsumm$cap[XX]`</em> column. It is ordered from your highest scored element to your lowest scored element.<br/>
&bull; Look at the <em>National Comparison</em> column. See if you scored <em>greater than, equivalent to, or less than</em> the national benchmark score for each element of `r db.capsumm$cap[XX]`. The <em>Action</em> column identifies assets and areas for growth.</p></div>
</td></tr>
<tr><td class='bg-light-green'>Your selected Cohort: <span style='color:black;'>Arts and Humanities</span></td></tr>
</table>
</div>

```{r tableoutput5, echo=FALSE}
#output table
table.builder(XX)

#build reflection items for output below
reflect.builder(db.capsumm$capacityCode[XX])
```

<div id='bttm-container' style="height:`r reflect.padding`in;">
<p style='margin-top:-.5em;font-size:12pt !important;' class='txt-larger-blue'>Reflection for Action:</p>
<ul>`r reflect.list`</ul>
</div>

<!----REPORT PAGE 6---->
```{r rpt-page-6, echo=FALSE}
XX = 6
```

<!--HEADER-->
<div id='header-container' class='break'>
<table id='header-table'>
<tr>
<td style="vertical-align: bottom;padding-bottom: .5em;width: 7in;"><p><strong>`r usr.name`</strong> `r format(Sys.Date(), '%m/%d/%Y')`</p>
<div style='width:3%;background-color:rgb(207,144,38);height:.5em;float:left;'></div><div style='width:97%;background-color:rgb(0,73,123);height:.5em;'></div>
</td>
<td><img src="C:/reports/images/index-primary-cropped.png" id='page-logo'/></td>
</tr>
<tr><td colspan='2'><h1>`r db.capsumm$cap[XX]`:</h1>
`r db.capsumm$cap_def[XX]`</td>
</tr>
</table>
</div>

<div id='top-container'>
<!--SCALE GRAPH-->
<div id='left-container'>
<div id='scale-descript-container'>`r db.capsumm$codified_scale[XX]`</div>
<!--BACKGROUND SCALE-->
<div id='scale-container'>
`r scale.builder(db.capsumm$scale[XX])`
<!--NATIONAL AVERAGE LINE-->
<div id='nat-avg-container' style="margin-top:-2.5in"><div class='nat-avg-label'>National Average</div><div class='nat-line'><hr class='nat-line-hr'/></div></div>
<!--SELF BAR-->
<div id='self-bar' class='value-bar'>
<div id='self-bar-color' class='bar-main-color'><div class='bar-background-color' style='height:1.3in'></div></div>
<div id='self-bar-value' style='margin-top:-2.8in;'><p class='bar-value-number'>1.90</p></div>
</div>
<!--COHORT BAR-->
<div id='cohort-bar' class='value-bar'>
<div id='cohort-bar-color' class='bar-main-color'><div class='bar-background-color' style='height:2.05in'></div></div>
<div id='cohort-bar-value' style='margin-top:-2in;'><p class='bar-value-number'>1.12</p></div>
</div>
<!--NATIONAL BAR-->
<div id='national-bar' class='value-bar'>
<div id='national-bar-color' class='bar-main-color'><div class='bar-background-color' style='height:2.15in'></div></div>
<div id='national-bar-value' style='margin-top:-1.85in;'><p class='bar-value-number'>1.09</p></div>
</div>
</div></div>
<!--INTERPRETATION & COHORT TEXT BOXES-->
<div id='right-container'>
<table>
<tr><td class='dark-blue-box'>Interpreting Your Results</td></tr>
<tr class='interpret-box'><td class='bg-light-blue' style='padding: .5em;'>
<h3 style='margin-top:-.05in;'>Grow it</h3>
<p><div class='round-number'>1</div><div class='interpret-txt-box'><span class='txt-light-blue'>Your overall results</span><br/>
Your score is <strong>equivalent to</strong> the national benchmark score for this capacity.</p></div>
<p><div class='round-number'>2</div><div class='interpret-txt-box'><span class='txt-light-blue'>Your selected cohort results</span><br/>
Your score is <strong>equivalent to</strong> the selected cohort benchmark score for this capacity.</p></div>
<p><div class='round-number'>3</div><div class='interpret-txt-box'><span class='txt-light-blue'>Developing elements of `r db.capsumm$cap[XX]`</span> <br/>
&bull; Look at the <em>Elements of `r db.capsumm$cap[XX]`</em> column. It is ordered from your highest scored element to your lowest scored element.<br/>
&bull; Look at the <em>National Comparison</em> column. See if you scored <em>greater than, equivalent to, or less than</em> the national benchmark score for each element of `r db.capsumm$cap[XX]`. The <em>Action</em> column identifies assets and areas for growth.</p></div>
</td></tr>
<tr><td class='bg-light-green'>Your selected Cohort: <span style='color:black;'>Arts and Humanities</span></td></tr>
</table>
</div>

```{r tableoutput6, echo=FALSE}
#output table
table.builder(XX)

#build reflection items for output below
reflect.builder(db.capsumm$capacityCode[XX])
```

<div id='bttm-container' style="height:`r reflect.padding`in;">
<p style='margin-top:-.5em;font-size:12pt !important;' class='txt-larger-blue'>Reflection for Action:</p>
<ul>`r reflect.list`</ul>
</div>


<!------CAPACITY-DEF PAGE------>
<!--HEADER-->
<div id='header-container' class='break'>
<table id='header-table'>
<tr>
<td style="vertical-align: bottom;padding-bottom: .5em;width: 7in;"><p><strong>`r usr.name`</strong> `r format(Sys.Date(), '%m/%d/%Y')`</p>
<div style='width:3%;background-color:rgb(207,144,38);height:.5em;float:left;'></div><div style='width:97%;background-color:rgb(0,73,123);height:.5em;'></div>
</td>
<td><img src="C:/reports/images/index-primary-cropped.png" id='page-logo'/></td>
</tr>
</table>
</div>
<h1 style='margin-top:-.70in'>Capacities & Definitions</h1>

```{r capdeftable, echo=FALSE}
htmlTable(db.allcaps[order(db.allcaps$cap),], header = c("Capacity", "Definition"), rnames=FALSE, align = 'l', css.class='cap-def-table')
```

